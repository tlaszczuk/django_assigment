from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User


class Entry(models.Model):
    title = models.CharField(max_length=80)
    owner = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    important = models.BooleanField(default=False)

    class Meta:
        ordering = ['title', 'created_at']
        verbose_name, verbose_name_plural = "entry", "entries"

    def __unicode__(self):
        return self.title
