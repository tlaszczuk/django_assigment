from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory

from .forms import EntryForm
from .models import Entry
from .views import create_entry


class EntryFormTestCase(TestCase):

    def test_unimportant_entry_validation(self):
        f = EntryForm({'title': 'TEST', 'body': 'TEST', 'important': False})
        self.assertTrue(f.is_valid())

    def test_important_entry_validation(self):
        f = EntryForm({'title': 'TEST', 'body': 'TEST', 'important': True})
        self.assertFalse(f.is_valid())

        f = EntryForm({'title': '[UWAGA]TEST', 'body': 'TEST',
                       'important': True})
        self.assertTrue(f.is_valid())


class EntryCreateViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.staff_member = User.objects.create_user(
            username='staff', email='staff@member.com', password='test'
        )
        self.staff_member.is_staff = True
        self.staff_member.save()

    def test_create_view_get_response_for_staff_member(self):
        request = self.factory.get('/entries/create/')
        request.user = self.staff_member
        response = create_entry(request)
        self.assertEqual(response.status_code, 200)

    def test_create_view_response_for_regular_user(self):
        regular_user = User.objects.create_user(
            username='regular', email='regular@member.com', password='test'
        )
        request = self.factory.get('/entries/create/')
        request.user = regular_user
        response = create_entry(request)
        self.assertEqual(response.status_code, 302)

    def test_saving_new_entry(self):
        self.assertEqual(Entry.objects.count(), 0)
        data = {'title': 'TEST', 'body': 'TEST', 'important': False}
        request = self.factory.post('/entries/create/', data=data)
        request.user = self.staff_member
        response = create_entry(request)
        self.assertEqual(Entry.objects.count(), 1)

    def test_valid_form_redirects(self):
        data = {'title': 'TEST', 'body': 'TEST', 'important': False}
        request = self.factory.post('/entries/create/', data=data)
        request.user = self.staff_member
        response = create_entry(request)
        self.assertEqual(response.status_code, 302)

    def test_valid_form_response(self):
        data = {'title': 'TEST', 'body': 'TEST', 'important': False}
        request = self.factory.post('/entries/create/', data=data)
        request.user = self.staff_member
        response = create_entry(request)
        self.assertIn("Dodano nowy wpis", response.content)

    def test_invalid_form_response(self):
        data = {'title': 'TEST', 'body': 'TEST', 'important': False}
        request = self.factory.post('/entries/create/', data=data)
        request.user = self.staff_member
        response = create_entry(request)
        self.assertIn("Brak [UWAGA] w tytule", response.content)


class EntryMiddleWareTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.staff_member = User.objects.create_user(
            username='staff', email='staff@member.com', password='test'
        )

    def test_entry_counter(self):
        request = self.factory.get('/entries/create/')
        request.user = self.staff_member
        response = create_entry(request)
        self.assertIn("0", response.content)

        entry = Entry.objects.create(owner=self.staff_member, title='TEST',
                                     body='TEST')
        self.assertIn("1", response.content)
